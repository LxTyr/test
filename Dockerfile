FROM node:8.12.0

MAINTAINER yb-fe

COPY . /app/

WORKDIR /app

RUN rm -rf node_modules
RUN npm install

EXPOSE 3000

CMD [ "npm", "run", "production"]
