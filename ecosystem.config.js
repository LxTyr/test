module.exports = {
  apps: [
    {
      name: 'yuanben-frontend-v2',
      script: 'npm start',
      env_staging: {
        YUANBEN_ENV: 'staging'
      },
      env_production: {
        YUANBEN_ENV: 'production'
      }
      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    }
  ]
}
