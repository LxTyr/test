const pkg = require('./package')
const path = require('path')
const loadEnv = require('./utils/loadEnv')
const merge = require('webpack-merge')

const nuxtConfig = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  server: {
    host: '0.0.0.0' // default: localhost,
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: 'red' },

  /*
  ** Global CSS
  */
  css: ['normalize.css', './styles/main.scss'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: ['~/plugins/axios'],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    debug: true
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

class NuxtService {
  constructor(mode = process.env.YUANBEN_ENV) {
    this.context = __dirname
    this.env = {}
    if (mode) {
      this.loadEnv(mode)
    }
    this.loadEnv()
    this.initConfig()
  }

  loadEnv(mode) {
    const basePath = path.resolve(this.context, `.env${mode ? `.${mode}` : ``}`)
    const localPath = `${basePath}.local`

    const load = path => {
      try {
        return loadEnv(path)
      } catch (err) {
        // only ignore error if file is not found
        if (err.toString().indexOf('ENOENT') < 0) {
          error(err)
        }
      }
    }

    this.env = merge(this.env, load(localPath), load(basePath))
  }

  initConfig() {
    console.log(this.env)
    this.nuxtConfig = merge({}, nuxtConfig, {
      env: this.env,
      axios: {
        baseURL: this.env.SERVER_ENDPOINT,
        credentials: true,
        proxy: {
          cookieDomainRewrite: ''
        }
      },
      proxy: {
        '/api/': {
          target: this.env.SERVER_ENDPOINT,
          pathRewrite: { '^/api/': '' }
        }
      }
    })
    if (process.env.NODE_ENV === 'production') {
      this.nuxtConfig = merge(this.nuxtConfig, {
        modules: [
          [
            './modules/baidu-analytics',
            { id: '8554b003ae7608e375558b88429cc5cd' }
          ]
        ]
      })
    }
  }

  export() {
    return this.nuxtConfig
  }
}

const service = new NuxtService()
module.exports = service.export()
