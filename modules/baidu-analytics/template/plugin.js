import Vue from 'vue'

function loadScript(url) {
  return new Promise((resolve, reject) => {
    var head = document.head || document.getElementsByTagName('head')[0]
    const script = document.createElement('script')
    script.async = true
    script.src = url
    script.charset = 'utf8'

    head.appendChild(script)

    script.onload = resolve
    script.onerror = reject
  })
}

function hasScript() {
  const scriptTags = Array.prototype.slice
    .call(document.getElementsByTagName('script'))
    .filter(script => {
      return script.src.indexOf('hm.js') !== -1
    })

  return scriptTags.length > 0
}

function autoTracking(router) {
  if (!router) {
    return
  }
  window['_hmt'].push(['_trackPageview', router.currentRoute.path]);
  router.afterEach(function (to, from) {

    if (to.path === from.path) {
      return
    }

    Vue.nextTick().then(() => {
      window['_hmt'].push(['_trackPageview', router.currentRoute.path]);
    })
  })
}

const analyticsPlugin = {
  install(Vue, options) {
    if (typeof document === 'undefined' || typeof window === 'undefined') {
      return
    }

    const resource = `//hm.baidu.com/hm.js?${options.id}`
    if (!hasScript() && !window['_hm']) {
      loadScript(resource).then(() => {
        autoTracking(options.router)
      })
    }
  }
}

export default async function({ app: { router } }) {
  const moduleOptions = <%= serialize(options) %>
  Vue.use(analyticsPlugin, { router , ...moduleOptions })
}
