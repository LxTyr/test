const { resolve } = require('path')

module.exports = function module(moduleOptions) {
  const options = moduleOptions
  this.addPlugin({
    src: resolve(__dirname, './template/plugin.js'),
    options
  })
}
